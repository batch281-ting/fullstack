
import { Container } from 'react-bootstrap';
import AppNavBar from './components/AppNavBar';
// import Banner from './components/Banner';
// import Highlights from './components/Highlights';
import Courses from './pages/Courses';
import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import './App.css';
import Logout from './pages/Logout';
import NotFound from './pages/NotFound';

import {useState, useEffect} from 'react';

import CourseView from './components/CourseView';

// import necessary modules from react-router-dom tht will be used for the routing


import {UserProvider} from './UserContext';

import {BrowserRouter, Route, Routes} from 'react-router-dom'; 

// React JS is a SPA
// Whenever a link is clicked, it functions as if the page is being reloaded but what actually happens is it goes through the process of rendering the page, mounting, rendering and remounting. 


function App() {
  // State hook for the user state that's defined here for a global scoping
  // Initialized as a the value of the user information from browser's localStorage
  // this will be used to store the user info, and will be used for validating if a user is logged in on the app or not

  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  });


  // Function is for clearing localStorage on logout
  const unsetUser = () => {
    localStorage.clear()
  };

  useEffect(()=> {
    console.log(user)
  }, [user]);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`,{
        headers:{
            Authorization: `Bearer ${localStorage.getItem('token')}`
        }
    })
    .then(response => response.json())
    .then(data => {
        setUser({
            id: data._id,
            isAdmin: data.isAdmin 
        })
    })
  }, [])



  return (
    <UserProvider value = {{user, setUser, unsetUser}}>  
      <BrowserRouter>
      <Routes>
        <Route
          path="/"
          element={
            <div>
              <AppNavBar />
              <Container>
                <Home />
              </Container>
            </div>
          }
        />
        <Route
          path="/courses"
          element={
            <div>
              <AppNavBar />
              <Container>
                <Courses />
              </Container>
            </div>
          }
        />
        <Route
          path="/register"
          element={
            <div>
              <AppNavBar />
              <Container>
                <Register />
              </Container>
            </div>
          }
        />
        <Route
          path="/login"
          element={
            <div>
              <AppNavBar />
              <Container>
                <Login />
              </Container>
            </div>
          }
        />

        <Route
          path="/courses/:id"
          element={
            <div>
              <AppNavBar />
              <Container>
                <CourseView />
              </Container>
            </div>
          }
        />

        <Route
          path="/logout"
          element={
            <div>
              <AppNavBar />
              <Container>
                <Logout />
              </Container>
            </div>
          }
        />
        <Route path="*" element={<NotFound />} />
      </Routes>
    </BrowserRouter>
    </UserProvider>
  );
}

export default App;
