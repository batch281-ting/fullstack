import React from "react";

// Create a Context Object
// A context object as the name states is  dt type of n object that can be used to store information that can be share shared within the app
// The context object is a different approach of passing information between components and allows easier access avoiding the us of props

// Using the createContext from react we can create a context in our app
// We contain the context created in our UserContext variable 
// We named it UserContext simply because this context will contain the information of the user.
const UserContext = React.createContext();

// The Provider component allows other components to consume/ user the context object and supply the necessary information needed to the context object
export const UserProvider = UserContext.Provider;

export default UserContext;