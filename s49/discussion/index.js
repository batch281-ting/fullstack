// Get post data
fetch('https://jsonplaceholder.typicode.com/posts')
  .then((response) => response.json())
  .then((data) => showPosts(data));

// Add post data
document.querySelector('#form-add-post').addEventListener('submit', (e) => {
  e.preventDefault();

  fetch('https://jsonplaceholder.typicode.com/posts', {
    method: 'POST',
    body: JSON.stringify({
      title: document.querySelector('#txt-title').value,
      body: document.querySelector('#txt-body').value,
      userId: 1,
    }),
    headers: { 'Content-type': 'application/json; charset=UTF-8' },
  })
    .then((response) => response.json())
    .then((data) => {
      console.log(data);
      alert('Successfully added');

      document.querySelector('#txt-title').value = '';
      document.querySelector('#txt-body').value = '';
    });
});

// Show posts
const showPosts = (posts) => {
  let postEntries = '';

  posts.forEach((post) => {
    postEntries += `
      <div id="post-${post.id}">
        <h3 id="post-title-${post.id}">${post.title}</h3>
        <p id="post-body-${post.id}">${post.body}</p>
        <button onclick="editPost('${post.id}')">Edit</button>
        <button onclick="deletePost('${post.id}')">Delete</button>
      </div>
    `;
  });

  document.querySelector('#div-post-entries').innerHTML = postEntries;
};


// Edit post
const editPost = (id) => {
	let title = document.querySelector(`#post-title-${id}`).innerHTML
	let body = document.querySelector(`#post-body-${id}`).innerHTML

	document.querySelector(`#post-title-id`).value = id;
	document.querySelector(`#post-edit-title`).value = title;
	document.querySelector(`#post-edit-body`).value = body;
	document.querySelector(`#post-submit-update`).removeAttribute('disabled');
}


// Update post

document.querySelector('#form-edit-post').addEventListener('submit', (e) => {
	e.preventDefault();

  	fetch('https://jsonplaceholder.typicode.com/posts', {
	    method: 'PUT',
	    body: JSON.stringify({
	      id : document.querySelector('#txt-edit-id').value,
	      title: document.querySelector('#txt-title').value,
	      body: document.querySelector('#txt-body').value,
	      userId: 1,
	    }),
	    headers: { 'Content-type': 'application/json; charset=UTF-8' },
    })
  	.then((response) => response.json())
  	.then((data) => {
  		console.log(data);
  		alert('Successfully Updated');

  		document.querySelector('#txt-edit-id').value = null;
  		document.querySelector('#txt-edit-title').value = null;
  		document.querySelector('#txt-edit-body').value = null;
  		document.querySelector('#btn-submit-update').setAttribute('disabled', true);
  	})
})


// Delete post
const deletePost = (id) => {
  fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, {
    method: 'DELETE',
  })
    .then(() => {
      const post = document.querySelector(`#post-${id}`);
      post.remove();
    });
    alert('Successfully deleted');
};
