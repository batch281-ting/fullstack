import React, { useEffect, useState } from 'react';
import FeaturedProductCard from '../components/FeaturedProductCard';

export default function FeaturedProducts() {
  const [products, setProducts] = useState([]);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/all`)
      .then(response => response.json())
      .then(data => {
        const slicedProducts = [data[0], data[2], data[3], data[4]];
        setProducts(slicedProducts.map(product => (
          <FeaturedProductCard key={product._id} product={product} />
        )));
      });
  }, []);

  return (
    <div className='row'>
      {products}
    </div>
  );
}
