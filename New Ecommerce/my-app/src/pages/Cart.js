import React, { useEffect, useState, useContext } from 'react';
import { useNavigate } from 'react-router-dom';
import '../App.css';
import { FiTrash2 } from 'react-icons/fi';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

// Rest of the code...


export default function CartData() {
  const [cartOrders, setCartOrders] = useState([]);
  const [totalAmount, setTotalAmount] = useState(0);
  const navigate = useNavigate();

  useEffect(() => {
    fetchCartOrders();
  }, []);

  const fetchCartOrders = () => {
    fetch(`${process.env.REACT_APP_API_URL}/users/cart/orders`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then((response) => {
        if (!response.ok) {
          throw new Error('Failed to fetch cart orders.');
        }
        return response.json();
      })
      .then((data) => {
        if (data.cartItems && data.totalAmount !== undefined) {
          setCartOrders(data.cartItems);
          setTotalAmount(data.totalAmount);
        } else {
          setCartOrders([]);
          setTotalAmount(0);
        }
      })
      .catch((error) => {
        console.error('Error fetching cart orders:', error);
      });
  };

  const removeFromCart = (productId) => {
    Swal.fire({
      title: 'Confirm Removal',
      text: 'Are you sure you want to remove this item from the cart?',
      icon: 'question',
      showCancelButton: true,
      confirmButtonText: 'Remove',
      cancelButtonText: 'Cancel',
    }).then((result) => {
      if (result.isConfirmed) {
        fetch(`${process.env.REACT_APP_API_URL}/users/cart/remove`, {
          method: 'DELETE',
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${localStorage.getItem('token')}`,
          },
          body: JSON.stringify({ productId }),
        })
          .then((response) => response.json())
          .then((data) => {
            if (data && data.hasOwnProperty('message')) {
              setCartOrders((prevOrders) =>
                prevOrders.filter((order) => order.productId !== productId)
              );
              Swal.fire({
                title: 'Success',
                text: 'Item removed from the cart!',
                icon: 'success',
              });
            } else {
              throw new Error('Failed to remove item from the cart.');
            }
          })
          .catch((error) => {
            console.error('Error removing item from cart:', error);
            Swal.fire({
              title: 'Error',
              text: 'Failed to remove item from the cart.',
              icon: 'error',
            });
          });
      }
    });
  };
  

  const updateQuantity = (productId, quantity) => {
    const updatedQuantity = parseInt(quantity, 10);
    if (isNaN(updatedQuantity) || updatedQuantity <= 0) {
      return;
    }

    fetch(`${process.env.REACT_APP_API_URL}/users/cart/update`, {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
      body: JSON.stringify({ productId, quantity: updatedQuantity }),
    })
      .then((response) => response.json())
      .then((data) => {
        if (data.success) {
          setCartOrders((prevOrders) =>
            prevOrders.map((order) =>
              order.productId === productId
                ? { ...order, quantity: updatedQuantity }
                : order
            )
          );
          Swal.fire({
            title: 'Success',
            text: 'Quantity updated!',
            icon: 'success',
          });
        } else {
          Swal.fire({
            title: 'Error',
            text: 'Failed to update quantity.',
            icon: 'error',
          });
        }
      })
      .catch((error) => {
        console.error('Error updating quantity:', error);
        Swal.fire({
          title: 'Error',
          text: 'Failed to update quantity.',
          icon: 'error',
        });
      });
  };

  const calculateTotal = () => {
    return cartOrders
      .reduce((total, order) => total + parseFloat(order.subtotal), 0)
      .toFixed(2);
  };

  const handleCheckout = () => {
    Swal.fire({
      title: 'Confirm Checkout',
      text: 'Are you sure you want to proceed with the checkout?',
      icon: 'question',
      showCancelButton: true,
      confirmButtonText: 'Checkout',
      cancelButtonText: 'Cancel',
    }).then((result) => {
      if (result.isConfirmed) {
        fetch(`${process.env.REACT_APP_API_URL}/orders/checkout`, {
          method: 'POST',
          headers: {
            'Content-type': 'application/json',
            'Authorization': `Bearer ${localStorage.getItem('token')}`
          },
        })
          .then(response => response.json())
          .then(data => {
            if (data === true) {
              Swal.fire({
                title: 'Checkout Successfull!',
                icon: 'success',
                text: 'Your products will be delivered soon.'
              })
              navigate('/orders');
            }
            if (data.status === 'empty') {
              Swal.fire({
                title: 'Your Cart is Empty!',
                icon: 'error',
                text: 'Check our products and add one to cart.'
              })
            }
          })
      }
  
      setCartOrders([]);
  
      Swal.fire({
        title: 'Success',
        text: 'Checkout completed!',
        icon: 'success',
      });
    });
  };
 
		


  return (
    <div>
      {cartOrders.length === 0 ? (
        <p className="empty-cart-message2">Your cart is empty.</p>
      ) : (
        <div>
          <h2 className="title-cart2">Cart Orders</h2>
          <div className="cart-items-container2">
            {cartOrders.map((order, index) => (
              <div key={`${order.productId}-${index}`} className="cart-item2">
                <div className="product-name2">{order.name}</div>
                <div className="product-description2">{order.description}</div>
                <div className="quantity-controls2">
                  <button
                    className="quantity-btn2"
                    onClick={() => updateQuantity(order.productId, order.quantity - 1)}
                  >
                    -
                  </button>
                  <input
                    type="tel"
                    className="quantity-input2"
                    value={order.quantity}
                    onChange={(e) => updateQuantity(order.productId, e.target.value)}
                  />
                  <button
                    className="quantity-btn2"
                    onClick={() => updateQuantity(order.productId, order.quantity + 1)}
                  >
                    +
                  </button>
                </div>
                <div className="product-subtotal2">{order.subtotal}</div>
                <div className="remove-icon2" onClick={() => removeFromCart(order.productId)}>
                  <FiTrash2 />
                </div>
              </div>
            ))}
          </div>
          <div className="cart-total2">Total: {calculateTotal()}</div>
          <div className="button-container2">
            <button className="checkout-button btn" onClick={handleCheckout}>
              Proceed to Checkout
            </button>
            <button className="continue-button btn" onClick={() => navigate('/')}>
            Continue Shopping
          </button>
          </div>
        </div>
      )}
    </div>
  );
}
