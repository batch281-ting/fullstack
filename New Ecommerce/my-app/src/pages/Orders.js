import React, { useEffect, useState } from 'react';
import '../App.css';

export default function Orders() {
  const [orders, setOrders] = useState([]);

  useEffect(() => {
    fetchOrders();
  }, []);

  const fetchOrders = () => {
    fetch(`${process.env.REACT_APP_API_URL}/orders/allorders`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then((response) => {
        if (!response.ok) {
          throw new Error('Failed to fetch orders.');
        }
        return response.json();
      })
      .then((data) => {
        if (data.success) {
          setOrders(data.orders);
        } else {
          console.error('Failed to fetch orders:', data.message);
        }
      })
      .catch((error) => {
        console.error('Error fetching orders:', error);
      });
  };

  return (
    <div>
      {orders.length === 0 ? (
        <p>No orders found.</p>
      ) : (
        <table className="order-table">
          <thead>
            <tr>
              <th>User</th>
              <th>Products</th>
              <th>Total Amount</th>
              <th>Purchased On</th>
            </tr>
          </thead>
          <tbody>
            {orders.map((order) => (
              <tr key={order._id}>
                <td>{order.userId && order.userId.lastName}</td>
                <td>
                  <ul>
                    {order.products.map((product) => (
                      <li key={product.productId && product.productId._id}>
                        {product.productId && product.productId.name}
                      </li>
                    ))}
                  </ul>
                </td>
                <td>{order.totalAmount}</td>
                <td>{order.purchasedOn}</td>
              </tr>
            ))}
          </tbody>
        </table>
      )}
    </div>
  );
}
