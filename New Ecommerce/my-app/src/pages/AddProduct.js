import React, { useState } from 'react';


export default function AddProduct() {
    const [product, setProduct] = useState({
        imageURL: '',
        name: '',
        description: '',
        price: '',
        isActive: true,
      });
      const [dragging, setDragging] = useState(false);
    
      const handleInputChange = (event) => {
        const { name, value } = event.target;
        setProduct((prevProduct) => ({ ...prevProduct, [name]: value }));
      };
    
      const handleDrop = (event) => {
        event.preventDefault();
        setDragging(false);
    
        const file = event.dataTransfer.files[0];
        const reader = new FileReader();
    
        reader.onloadend = () => {
          setProduct((prevProduct) => ({
            ...prevProduct,
            imageURL: reader.result,
          }));
        };
    
        reader.readAsDataURL(file);
      };
    
      const handleDragOver = (event) => {
        event.preventDefault();
        setDragging(true);
      };
    
      const handleDragLeave = () => {
        setDragging(false);
      };
    
      const handleCancelUpload = () => {
        setProduct((prevProduct) => ({ ...prevProduct, imageURL: '' }));
      };
    
      const handleSubmit = (event) => {
        event.preventDefault();
    
        fetch(`${process.env.REACT_APP_API_URL}/products`, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${localStorage.getItem('token')}`,
          },
          body: JSON.stringify(product),
        })
          .then((response) => response.text())
          .then((data) => {
            console.log(data);
            // Handle success or display an alert
          })
          .catch((error) => {
            console.error('Error:', error);
          });
      };
    
      return (
        <div className="add-product-container">
          <h2>Add Product</h2>
          <form className="product-form" onSubmit={handleSubmit}>
            <div className="image-container">
              <div className="image-preview">
                {product.imageURL && (
                  <img src={product.imageURL} alt="Product" className="preview-image" />
                )}
              </div>
              {product.imageURL && (
                <button
                  type="button"
                  onClick={handleCancelUpload}
                  className="cancel-upload-button"
                >
                  Cancel Upload
                </button>
              )}
              {!product.imageURL && (
                <div
                  className={`drag-drop-area ${dragging ? 'dragging' : ''}`}
                  onDrop={handleDrop}
                  onDragOver={handleDragOver}
                  onDragLeave={handleDragLeave}
                >
                  <p>Drag and drop an image here</p>
                </div>
              )}
            </div>
            <div className="label-container">
                <label>
                    Name:
                    <input
                    type="text"
                    name="name"
                    value={product.name}
                    onChange={handleInputChange}
                    className="product-input"
                    />
                </label>
                <label>
                    Description:
                    <textarea
                    name="description"
                    value={product.description}
                    onChange={handleInputChange}
                    className="product-input"
                    ></textarea>
                </label>
                <label>
                    Price:
                    <input
                    type="number"
                    name="price"
                    value={product.price}
                    onChange={handleInputChange}
                    className="product-input"
                    />
                </label>
                </div>

            <button type="submit" className="product-button">
              Add Product
            </button>
          </form>
        </div>
      );
              }      
    