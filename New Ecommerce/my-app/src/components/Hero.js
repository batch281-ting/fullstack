import { LiaShippingFastSolid } from 'react-icons';


import React from 'react';


export default function Hero(){
    return(
        <section className="hero p-2">
        <div className='container-xxl'>
            <div className='row'>
                <div className='hero-details d-flex justify-content-around text-center '>
                    <div className='card-details mx-3'>
                        <div className='card p-2'>
                            <img src='https://static.vecteezy.com/system/resources/thumbnails/002/206/240/small/fast-delivery-icon-free-vector.jpg' alt="" className='img-fluid m-auto' />
                            <p>Fast Delivery</p>
                        </div>    
                    </div>
                    <div className='card-details mx-3'>
                        <div className='card p-2'>
                            <img src='https://t4.ftcdn.net/jpg/05/59/98/19/360_F_559981992_WvY9zu7esrHgJ8gFcmGqK8QetBeN9ImF.jpg' alt="" className='img-fluid m-auto' />
                            <p>24/7 Support</p>
                        </div>
                    </div>
                    <div className='card-details mx-3'>
                        <div className='card p-2'>
                            <img src='https://static.thenounproject.com/png/248469-200.png' alt="" className='img-fluid m-auto' />
                            <p>Discounts</p>
                        </div>
                        </div>
                    

                </div>
            </div>
        </div>
    </section>
    )
}