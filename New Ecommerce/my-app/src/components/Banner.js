import { Carousel } from 'react-bootstrap';
import '../App.css';

export default function Banner() {
  return (
    <div className="w-100 carousel-wrapper">
      <Carousel>
        <Carousel.Item interval={1000}>
          <img
            className="d-block w-100"
            src="https://c1.wallpaperflare.com/preview/755/46/382/shoes-boots-nike-air.jpg"
            alt="First slide"
          />
        </Carousel.Item>
        <Carousel.Item interval={500}>
          <img
            className="d-block w-100"
            src="https://c1.wallpaperflare.com/preview/585/370/573/various-footwear-shoe-shoes.jpg"
            alt="Second slide"
          />
        </Carousel.Item>
        <Carousel.Item>
          <img
            className="d-block w-100"
            src="https://c1.wallpaperflare.com/preview/350/200/60/grey-sport-shoes-sneakers.jpg"
            alt="Third slide"
          />
        </Carousel.Item>
      </Carousel>
    </div>
  );
}
